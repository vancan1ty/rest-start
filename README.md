# rest-start

This archetype will build you a basic RESTful webservice using Jetty and Jersey.

# usage example

Follow the following steps to use it:

1. check out the project
        
        git clone https://gitlab.com/vancan1ty/rest-start
        cd ./rest-start

2. install the archetype to your local maven repo, cd out of the rest-start directory
    
        mvn install archetype:update-local-catalog archetype:crawl
        cd ..

3. generate a new project interactively using the archetype (it will be listed
in your archetype options)
    
        mvn archetype:generate
        # select the com.cvberry:rest-start-template-archetype from the list either by number or groupId/artifactId
        # then specify 'groupId', 'artifactId', 'version', and 'package' as prompted.
    
    below is an example interactive archetype:generate session

        1830: local -> com.cvberry:rest-start-template-archetype (rest-start-template)
        Choose a number or apply filter (format: [groupId:]artifactId, case sensitive contains): 966: 1830
        Define value for property 'groupId': com.foo
        Define value for property 'artifactId': supertest
        Define value for property 'version' 1.0-SNAPSHOT: : 
        Define value for property 'package' com.foo: : 
        [INFO] Using property: excludePatterns = Makefile,archetypePFile.txt,target,REAL_README.md,rest-start-template.iml
        Confirm properties configuration:
        groupId: com.foo
        artifactId: supertest
        version: 1.0-SNAPSHOT
        package: com.foo
        excludePatterns: Makefile,archetypePFile.txt,target,REAL_README.md,rest-start-template.iml
        Y: : Y

4. build and run your new app
    
        cd ./supertest
        mvn clean package
        java -jar target/supertest-1.0-SNAPSHOT.jar

5. see your api in action

    open the following link in your web browser

        http://localhost:8080/nextPrime?startNum=1024
    
5. run junit tests (optional)
    
        mvn test
    
    

